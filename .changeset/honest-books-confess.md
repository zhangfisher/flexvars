---
"flexvars": patch
---

feat: 当没有提供插值变量时返回保留原始字符串，如：`flexvars.replace("I am {}") => "I am {}"`
```
